#include<stdlib.h>
#include<fstream>
#include <iostream>
#include<iomanip>
#include<string>
#include<dirent.h> //for files and folders access.
#include<unistd.h>
//#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/stitching/stitcher.hpp"
#include <stdio.h>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/calib3d/calib3d.hpp"


using namespace std;
using namespace cv;

bool try_use_gpu = false;
vector<Mat> imgs;

int main(int argc, char* argv[])
{ 
  DIR *pdir = NULL;
  struct dirent *pent = NULL;
  pdir = opendir ("/home/ashraf/BriskSlam_12_19/Plane_Image/");
  if (pdir == NULL) 
    { 
        cout << "\nERROR! pdir could not be initialised correctly";
        exit (3);
    } 

 
    int count=0; //for counting number of folders
    while (pent = readdir (pdir))
    {
        if (pent == NULL) 
        { 
            cout << "\nERROR! pent could not be initialised correctly";
            exit (3);
        }

        std::cout << pent->d_name << std::endl;
	count++;
	
    }
    std::cout<<"count"<<count<<std::endl;
 
      
    closedir (pdir);
    cin.get (); // pause for input
    //usleep(5000);
    //return EXIT_SUCCESS; // everything went OK


 
  
  
  
  
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  int FolderNumber = count-3; // becuase (.) and (..) current and previous directories are listed
  
  for(int i=0;i<(FolderNumber);i++)
  {	
    cout<<"i is"<< i<< endl;
    vector<String> filenames; 
    std:: stringstream Folder;
    Folder << "/home/ashraf/BriskSlam_12_19/Plane_Image/Plane_"<<i<<"/"<<"*.png";
    std::stringstream result_name;
    result_name << "/home/ashraf/BriskSlam_12_19/Plane_Image/result/result_"<<i<<".jpg";
    glob(Folder.str().c_str(), filenames); 
    
    
    Mat src[filenames.size()];
    vector<Mat>imageList;
    
    
    for(size_t i = 0; i < filenames.size(); ++i)
    
    {
         src[i] = imread(filenames[i]);
	 
        if(!src[i].data)
            cout << "Problem loading image!!!" << endl;
	
	
	imshow("whatever",src[i]);
	imageList.push_back(src[i]);
	waitKey(0);
        
    } 
	std::cout<<"size of image vector "<<imageList.size()<<endl;
     
    Mat pano;
    Stitcher stitcher = Stitcher::createDefault(try_use_gpu);
	/*stitcher.setRegistrationResol(-1); /// 0.6
	stitcher.setSeamEstimationResol(-1);   /// 0.1
	stitcher.setCompositingResol(-1);   //1
	stitcher.setPanoConfidenceThresh(-1);   //1
	stitcher.setWaveCorrection(true);
	stitcher.setWaveCorrectKind(detail::WAVE_CORRECT_HORIZ);*/
    
     Stitcher::Status status = stitcher.stitch(imageList, pano);
    

    if (status != Stitcher::OK)
    {
        cout << "Can't stitch images, error code = " << int(status) << endl;
        return -1;
    } 

    imwrite(result_name.str().c_str(), pano);
  } 
    return 0;
} 
